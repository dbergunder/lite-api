<?php
/**
 * Bootstrap and Front Controller for API Calls
 *
 * Autoloader loads classes based on namespace
 *
 * Routes are loaded through the ini/routes.json file
 * Seperate route configurations can be loaded in this method, based on the config.ini settings
 *
 * Auth object is created and passed to the Service Abstraction Layer, which verifies Authorization
 * and runs the route matching
 *
 * Once a route is matched, ResourceFactory generates the resource object for the API, and that object
 * initializes any business logic or database calls needed to generate the response
 *
 * Errors are captured as exceptions, and an error response is generated based on the appropriate
 * status code
 */

//Autoloading
require_once __DIR__ . DIRECTORY_SEPARATOR . 'autoloader.php';

$classLoader = new SplClassLoader('lite');
$classLoader->register();

use lite\api\library\Router as Router;
use lite\api\library\Auth\Test as Test;
use lite\api\library\SAL\INTERNAL as INTERNAL;

/*
 * apiState is a placeholder.  We need to get configuration information 
 * to initialize the correct states from the config.ini file
*/
$apiState = 'development';

//Routes object initialized
$router = new Router();

//Read Global Application Config
$config = parse_ini_file("lite/api/ini/config.ini", true);

//Based on External, Internal, or other Service Type Request
if($apiState == 'development') {

	//Test Home Route
	$router->addRoute('GET', '/', function() use ($config) {
		echo "Hello World";
		//phpinfo();
	});

	//Internal Auth
	$auth = new TEST($config[$apiState]); //AUTH type

	//Route Config File
	$routeConfig = json_decode(file_get_contents($config[$apiState]['routes']), true);

	//Push Prefix off the top of the array.
	$prefixVersion = array_shift($routeConfig);

	//Add each route to the router
	foreach($routeConfig as $httpVerb => &$list) {
		foreach($list as $url => $resource) {
			$router->addRoute($httpVerb, $prefixVersion . $url, $resource['resource'], $resource['method']);
		}
	}
	$config[$apiState]['prefix'] = $prefixVersion;

	//Initialize Service Abstraction Layer object
	$gatekeeper = new INTERNAL($router, $auth, $config[$apiState]);
}

//Initialize API through SAL calls
$gatekeeper->run();

/** EOF **/