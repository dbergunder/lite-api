# lite-api prototype #

Based on other popular frameworks, light weight, with simple router to resource mapping.

Updated 22 Sep 2014 / Still needs work for composer and PSR compliance.

Router based on "we love php" blog post:
http://we-love-php.blogspot.de/2012/07/how-to-write-really-small-and-fast.html

Requires PHP >= 5.4
Tested on APACHE and Google App Engine