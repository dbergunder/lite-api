<?php
/**
 * @file api/resources/User.php
 *
 * User Resource for lite API
 */
namespace lite\api\resources;

use lite\api\library\Resource as Resource;
use lite\api\models\UserModel as UserModel;

/**
 * User API Endpoint
 */
class User extends Resource {
	private $model;

	public function __construct($config) {
		//$this->model = new UserModel($config);
		//Call parent constructor
		parent::__construct($config);
	}

	/**
	 * Returns specific user Resource response or current logged int user
	 * @api
	 * @param  array $args Params passed
	 * @return void       
	 */
	public function getUser($args){
		if(!empty($args) && isset($args['id'])) {
			$this->responseJSON("Get User resource with ID of " . $args['id']);
		}
		else {
			$this->getCurrentUser($args);
		}
	}

	/**
	 * Returns currently logged in user Resource response
	 * @api
	 * @param  array $args Params passed 
	 * @return void       responseJSON
	 */
	public function getCurrentUser($args) {
		if($_SESSION['user'] !== null) {
			/*
			$this->responseResourceJSON(
				array(
					"external_id" => $_SESSION['user'][0]['uid'], 
					"user_id" => $_SESSION['user']['user_id'],
					"href" => $this->_config['prefix'] . 'user/'.$_SESSION['user']['user_id'],
					"first_name" => $_SESSION['user'][0]['givenname'], 
					"last_name" => $_SESSION['user'][0]['sn'],
					"type" => 'permissions'
					),
				$_SESSION['user']['permissions']
				);*/
		}
		$this->responseResourceJSON(array('test' => true));
	}

	/**
	 * Get User Role
	 * @api
	 * @param  array $args expecting ID from URI
	 * @return void       
	 */
	public function getRole($args){
		if(!empty($args) && isset($args['id'])) {
			$this->responseJSON("Get Role of user with " . $args['id']);
		}

		$this->responseJSON("Return self Roles");
	}

	/**
	 * Recalculates User Permissions, stores in session and returns permission
	 * @api
	 * @param  array $args expecting ID from URI
	 * @return void       responseJSON
	 */
	public function getPermission($args){
		if(!empty($args) && isset($args['id'])) {
			//Recalculate Permissions and update Session
			$_SESSION['user']['permissions'] = $this->model->getUserAcl($args['id']);
			$this->responseJSON(array('permissions' => $_SESSION['user']['permissions']));
		}
		else{
			$this->response(400);
		}
	}

}
/** EOF **/
