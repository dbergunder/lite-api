<?php
/**
 * @file api/resources/Error.php
 *
 * Error Resource for lite API
 * TODO: logging/capturing in this class
 */
namespace lite\api\resources;

use lite\api\library\Resource as Resource;

/**
 * Error API Endpoint
 */
class Error extends Resource{

}
/** EOF **/
