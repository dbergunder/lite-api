<?php
/**
 * @file api/resources/Auth.php
 *
 * Authorize Resource for lite API for logging in and out
 */
namespace lite\api\resources;

use lite\api\library\Resource as Resource;
use lite\api\resources\User as User;
use lite\api\core\ACL as ACL;

/**
 * Endpoint for Authorize requests
 */
class Authorize extends Resource {
	public function __construct($config) {
		//Call parent constructor
		parent::__construct($config);
	}

	/**
	 * API Login resource endpoint, after user is authorized this generates additional
	 * user information such as ACL
	 * @api 
	 * @param  array $args Passed Arguements
	 * @return void
	 */
	public function logIn($args) {
		$external_id = $_SESSION['user'][0]['uid'];

		//New ACL object to register new users
		/*
		$ACL = new ACL($this->_config);
		$user_id = $ACL->register($external_id);

		$_SESSION['user']['user_id'] = $user_id;
		$_SESSION['user']['permissions'] = $ACL->getUserAcl($user_id);*/

		//Return User Object employid, user_id, first name, last name
		$user = new User($this->_config);
		$user->getCurrentUser($args);
	}

	/**
	 * API Manual Logout resource endpoint
	 * @api
	 * @param  array $args Passed Arguements
	 * @return void       
	 */
	public function logOut($args) {
		$_SESSION['user'] = null;
		$this->responseJSON(true);
	}

}
/** EOF **/