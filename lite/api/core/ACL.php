<?php
/** 
 * Access Control List
 * @file api/core/ACL.php
 * Permissoin/Role/Group 
 *
 * @usage
   //ACL permissions
	$acl = new lite\api\core\ACL($config['internal']);
	$permissions = $acl->getUserAcl(0); //Get from session
	var_dump($acl->hasPermission($permissions, 5, 1, 1, 1)); //bool response
 */
namespace lite\api\core;
use lite\api\models\UserModel as UserModel;

/** 
 * ACL class
 */
class ACL {
	protected $_config;

	public function __construct($config) {
		$this->_config = $config;
	}

	/**
	 * Return ACL Permissions for User
	 * @param  int $user_id User ID
	 * @return array          ACL array
	 */
	public function getUserAcl($user_id) {
		$userModel = new UserModel($this->_config);
		return $userModel->getUserAcl($user_id);
	}

	/**
	 * Verifys action against user permissions
	 * @param  array  $permissions   List of user permissions
	 * @param  int  $role_id       ID of role
	 * @param  int  $permission_id ID of permission
	 * @return boolean        
	 */
	public function hasPermission($permissions, $role_id, $permission_id) {
		$return = false;

		//Basic Hash search
		//Worst case needs to search entire array for false return
		foreach($permissions as $key => $value) {
			if($value['role_id'] == $role_id && $value['permission_id'] == $permission_id) {
				$return = true;
				break;
			}
		}
		return $return;
	}
}
/** EOF **/
