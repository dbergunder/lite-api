<?php
/**
 * @file api/models/UserModel.php
 *
 */
namespace lite\api\models;

use lite\api\library\Model as Model;

/**
 * User Model
 */
class UserModel extends Model {

	public function __construct(&$config) {
		parent::__construct($config);
	}

	public function getUser($id) {
		//PDO Select Statement
		$this->query = $this->_db->prepare("SELECT user_id FROM `user` WHERE `user_id` = :id");

		//Bind $id Param
		$this->query->bindValue(":id", $id, \PDO::PARAM_INT);

		//Execute Query
		return $this->executeFetchAll();
	}

	public function findUser($user_id) {
		//PDO Select Statement
		$this->query = $this->_db->prepare("SELECT user_id FROM `user` WHERE `user_id` = :id");

		//Bind $id Param
		$this->query->bindValue(":id", $user_id, \PDO::PARAM_INT);

		//Execute Query
		return $this->executeFetchAll();
	}

	public function insertUser($params) {
		//Prepare inserts to columns
		$columnValues = $this->prepareInsertColumnValues($params);

		//PDO Insert Statement
		$this->query = $this->_db->prepare("INSERT INTO user(" . $columnValues['columns'] . ") VALUES (" . $columnValues['values'] . ")");

		//Execute Query
		return $this->executeInsert();		
	}
}

/** EOF */