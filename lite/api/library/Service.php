<?php
 /*
 * Services lite will need to interface to
 * cURL builder for external service access
 */
namespace lite\api\library;

/**
 * cURL wrapper
 */
abstract class Service {
    /** cURL settings */
	public $follow_redirects = true; //Bool to follow redirects
    public $get_response_headers = true; //Bool to get headers from response
    public $referer; //The referer header to send along with requests
	public $user_agent; //The user agent to send along with requests 
	public $headers = array(); //Associative aray of headers to send with request
    public $requestHeaders = array(); //Requested headers after response

    /** Configuration Settings */
    protected $_config;

    /** cURL security settings */
    private $userpwd;

	public function __construct(&$config){
        $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
		$this->_config = $config;
	}

    /**
     * Makes an HTTP GET request to the specified $url with an optional array or string of $vars
     * Returns a raw response if the request was successful, false otherwise
     * @param  string $url URL to curl
     * @param  array $vars Params to put/post
     * 
     * @return response Curl response
    */
    public function get($url, $vars = array()) {
        if (!empty($vars)) {
            $url .= (stripos($url, '?') !== false) ? '&' : '?';
            $url .= (is_string($vars)) ? $vars : http_build_query($vars, '', '&');
        }
        return $this->request('GET', $url);
    }

    /**
     * Makes an HTTP POST request to the specified $url with an optional array or string of $vars
	 * Returns a raw response if the request was successful, false otherwise
     * @param  string $url URL to curl
     * @param  array $vars Params to put/post
     * 
     * @return response Curl response 
    */
    public function post($url, $vars = array(), $enctype = NULL) {
        return $this->request('POST', $url, $vars, $enctype);
    }

    /**
     * Makes an HTTP PUT request to the specified $url with an optional array or string of $vars
     * Returns a raw response if the request was successful, false otherwise
     * @param  string $url URL to curl
     * @param  array $vars Params to put/post
     * 
     * @return response Curl response
    */
    public function put($url, $vars = array()) {
        return $this->request('PUT', $url, $vars);
    }

    /**
     * Makes an HTTP DELETE request to the specified $url with an optional array or string of $vars
     * Returns a raw response if the request was successful, false otherwise
     * @param  string $url URL to curl
     * @param  array $vars Params to put/post
     * 
     * @return response Curl response
    */
    public function delete($url, $vars = array()) {
        return $this->request('DELETE', $url, $vars);
    }

    /**
     * Makes an HTTP request of the specified $method to a $url with an optional array or string of $vars
     * Returns a raw response if the request was successful, false otherwise
     * @param string $method HTTP method
     * @param string $url URL to curl
     * @param array $vars Params to pass via put/post
     * @param string $enctype Encryption Type
     *
     * @return response Curl Response
    */
    protected function request($method, $url, $vars = array(), $enctype = null) {
        $this->request = curl_init();
        
        //Proper encoding if not application/x-www-form-urlencoded
        if (is_array($vars) && $enctype != 'multipart/form-data') {
            $vars = http_build_query($vars, '', '&');
        }

        $this->set_request_method($method);
        $this->set_request_options($url, $vars); // if vars are passed POSTFIELDS are set

        if(!empty($this->requestHeaders)) {
            $this->set_request_headers();
        }

        $response = curl_exec($this->request);
        if (!$response) {
            //throw new CurlException(curl_error($this->request), curl_errno($this->request));
            echo curl_error($this->request) . '<br>';
        }

        if($this->get_response_headers) {
            $headerSize = curl_getinfo($this->request, CURLINFO_HEADER_SIZE);
            $header = substr($response, 0, $headerSize);
            $this->headers = $this->http_parse_headers($header);
        }

        curl_close($this->request);
        return $response;
    }

    /**
     * Set the associated CURL options for a request method
     * @param string $method HTTP method
     */
    protected function set_request_method($method) {
        switch (strtoupper($method)) {
            case 'HEAD':
                curl_setopt($this->request, CURLOPT_NOBODY, true);
                break;
            case 'GET':
                curl_setopt($this->request, CURLOPT_HTTPGET, true);
                break;
            case 'POST':
                curl_setopt($this->request, CURLOPT_POST, true);
                break;
            default:
                curl_setopt($this->request, CURLOPT_CUSTOMREQUEST, $method);
        }
    }

    /**
     * Sets the CURLOPT options for the current request
     * @param string $url  URL to curl
     * @param string $vars encoded parameters for POST
     */
    protected function set_request_options($url, $vars) {
        curl_setopt($this->request, CURLOPT_URL, $url);
        if (!empty($vars)) curl_setopt($this->request, CURLOPT_POSTFIELDS, $vars);
        
        // Set some default CURL options
        if($this->get_response_headers) {
            curl_setopt($this->request, CURLOPT_HEADER, true);
        }

        curl_setopt($this->request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->request, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($this->request, CURLOPT_USERAGENT, $this->user_agent);
      
        if ($this->follow_redirects) { curl_setopt($this->request, CURLOPT_FOLLOWLOCATION, true); }
        if ($this->referer) { curl_setopt($this->request, CURLOPT_REFERER, $this->referer); }
        if ($this->userpwd) {
            curl_setopt($this->request, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($this->request, CURLOPT_USERPWD, $this->userpwd); 
        } 
        else {
            curl_setopt($this->request, CURLOPT_HTTPAUTH, false);
        }
    }

    /*
    * Formats and adds custom headers to the current request
    */
    protected function set_request_headers() {
        $headers = array();
        foreach ($this->requestHeaders as $key => $value) {
            $headers[] = $key.': '.$value;
        }

        curl_setopt($this->request, CURLOPT_HTTPHEADER, $headers);
    }

    /**
     * parses raw response headers into array
     * @param  string $raw_headers Raw http headers
     * @return array              
     */
    protected function http_parse_headers($rawHeaders) {
        $headers = [];

        foreach (explode("\n", $rawHeaders) as $i => $h) {
            $h = explode(':', $h, 2);
            
            if (isset($h[1])) {
                $headers[$h[0]] = trim($h[1]);
            }
        }
        
        return $headers;
    }
}

/*
 * Error Handling for cURL
 */
class CurlException extends \Exception
{	
	static public $curl_errors = array(
		CURLE_ABORTED_BY_CALLBACK => 'CURLE_ABORTED_BY_CALLBACK',
		CURLE_BAD_CALLING_ORDER => 'CURLE_BAD_CALLING_ORDER',
		CURLE_BAD_CONTENT_ENCODING => 'CURLE_BAD_CONTENT_ENCODING',
		CURLE_BAD_FUNCTION_ARGUMENT => 'CURLE_BAD_FUNCTION_ARGUMENT',
		CURLE_BAD_PASSWORD_ENTERED => 'CURLE_BAD_PASSWORD_ENTERED',
		CURLE_COULDNT_CONNECT => 'CURLE_COULDNT_CONNECT',
		CURLE_COULDNT_RESOLVE_HOST => 'CURLE_COULDNT_RESOLVE_HOST',
		CURLE_COULDNT_RESOLVE_PROXY => 'CURLE_COULDNT_RESOLVE_PROXY',
		CURLE_FAILED_INIT => 'CURLE_FAILED_INIT',
		CURLE_FILE_COULDNT_READ_FILE => 'CURLE_FILE_COULDNT_READ_FILE',
		CURLE_FILESIZE_EXCEEDED => 'CURLE_FILESIZE_EXCEEDED',
		CURLE_FTP_ACCESS_DENIED => 'CURLE_FTP_ACCESS_DENIED',
		CURLE_FTP_BAD_DOWNLOAD_RESUME => 'CURLE_FTP_BAD_DOWNLOAD_RESUME',
		CURLE_FTP_CANT_GET_HOST => 'CURLE_FTP_CANT_GET_HOST',
		CURLE_FTP_CANT_RECONNECT => 'CURLE_FTP_CANT_RECONNECT',
		CURLE_FTP_COULDNT_GET_SIZE => 'CURLE_FTP_COULDNT_GET_SIZE',
		CURLE_FTP_COULDNT_RETR_FILE => 'CURLE_FTP_COULDNT_RETR_FILE',
		CURLE_FTP_COULDNT_SET_ASCII => 'CURLE_FTP_COULDNT_SET_ASCII',
		CURLE_FTP_COULDNT_SET_BINARY => 'CURLE_FTP_COULDNT_SET_BINARY',
		CURLE_FTP_COULDNT_STOR_FILE => 'CURLE_FTP_COULDNT_STOR_FILE',
		CURLE_FTP_COULDNT_USE_REST => 'CURLE_FTP_COULDNT_USE_REST',
		CURLE_FTP_PORT_FAILED => 'CURLE_FTP_PORT_FAILED',
		CURLE_FTP_QUOTE_ERROR => 'CURLE_FTP_QUOTE_ERROR',
		CURLE_FTP_SSL_FAILED => 'CURLE_FTP_SSL_FAILED',
		CURLE_FTP_USER_PASSWORD_INCORRECT => 'CURLE_FTP_USER_PASSWORD_INCORRECT',
		CURLE_FTP_WEIRD_227_FORMAT => 'CURLE_FTP_WEIRD_227_FORMAT',
		CURLE_FTP_WEIRD_PASS_REPLY => 'CURLE_FTP_WEIRD_PASS_REPLY',
		CURLE_FTP_WEIRD_PASV_REPLY => 'CURLE_FTP_WEIRD_PASV_REPLY',
		CURLE_FTP_WEIRD_SERVER_REPLY => 'CURLE_FTP_WEIRD_SERVER_REPLY',
		CURLE_FTP_WEIRD_USER_REPLY => 'CURLE_FTP_WEIRD_USER_REPLY',
		CURLE_FTP_WRITE_ERROR => 'CURLE_FTP_WRITE_ERROR',
		CURLE_FUNCTION_NOT_FOUND => 'CURLE_FUNCTION_NOT_FOUND',
		CURLE_GOT_NOTHING => 'CURLE_GOT_NOTHING',
		CURLE_HTTP_NOT_FOUND => 'CURLE_HTTP_NOT_FOUND',
		CURLE_HTTP_PORT_FAILED => 'CURLE_HTTP_PORT_FAILED',
		CURLE_HTTP_POST_ERROR => 'CURLE_HTTP_POST_ERROR',
		CURLE_HTTP_RANGE_ERROR => 'CURLE_HTTP_RANGE_ERROR',
		CURLE_LDAP_CANNOT_BIND => 'CURLE_LDAP_CANNOT_BIND',
		CURLE_LDAP_INVALID_URL => 'CURLE_LDAP_INVALID_URL',
		CURLE_LDAP_SEARCH_FAILED => 'CURLE_LDAP_SEARCH_FAILED',
		CURLE_LIBRARY_NOT_FOUND => 'CURLE_LIBRARY_NOT_FOUND',
		CURLE_MALFORMAT_USER => 'CURLE_MALFORMAT_USER',
		CURLE_OBSOLETE => 'CURLE_OBSOLETE',
		CURLE_OPERATION_TIMEOUTED => 'CURLE_OPERATION_TIMEOUTED',
		CURLE_OUT_OF_MEMORY => 'CURLE_OUT_OF_MEMORY',
		CURLE_PARTIAL_FILE => 'CURLE_PARTIAL_FILE',
		CURLE_READ_ERROR => 'CURLE_READ_ERROR',
		CURLE_RECV_ERROR => 'CURLE_RECV_ERROR',
		CURLE_SEND_ERROR => 'CURLE_SEND_ERROR',
		CURLE_SHARE_IN_USE => 'CURLE_SHARE_IN_USE',
		CURLE_SSH => 'CURLE_SSH',
		CURLE_SSL_CACERT => 'CURLE_SSL_CACERT',
		CURLE_SSL_CERTPROBLEM => 'CURLE_SSL_CERTPROBLEM',
		CURLE_SSL_CIPHER => 'CURLE_SSL_CIPHER',
		CURLE_SSL_CONNECT_ERROR => 'CURLE_SSL_CONNECT_ERROR',
		CURLE_SSL_ENGINE_NOTFOUND => 'CURLE_SSL_ENGINE_NOTFOUND',
		CURLE_SSL_ENGINE_SETFAILED => 'CURLE_SSL_ENGINE_SETFAILED',
		CURLE_SSL_PEER_CERTIFICATE => 'CURLE_SSL_PEER_CERTIFICATE',
		CURLE_TELNET_OPTION_SYNTAX => 'CURLE_TELNET_OPTION_SYNTAX',
		CURLE_TOO_MANY_REDIRECTS => 'CURLE_TOO_MANY_REDIRECTS',
		CURLE_UNKNOWN_TELNET_OPTION => 'CURLE_UNKNOWN_TELNET_OPTION',
		CURLE_UNSUPPORTED_PROTOCOL => 'CURLE_UNSUPPORTED_PROTOCOL',
		CURLE_URL_MALFORMAT => 'CURLE_URL_MALFORMAT',
		CURLE_URL_MALFORMAT_USER => 'CURLE_URL_MALFORMAT_USER',
		CURLE_WRITE_ERROR => 'CURLE_WRITE_ERROR'
	);
	function __construct( $curl_error_message, $curl_error_code )
	{
		if( ! array_key_exists( $curl_error_code, self::$curl_errors ) )
			throw new \Exception( "Unknown \$curl_error_code: $curl_error_code" );
		
		parent::__construct( self::$curl_errors[$curl_error_code].": $curl_error_message", $curl_error_code );
	}
}

/** EOF **/
