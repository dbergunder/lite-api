<?php
/**
 * Service Abstraction Layer
 *
 * Gatekeeper for all API requests, verifys authentication and routes to correct resource
 */
namespace lite\api\library;

use lite\api\resources\Error as Error;

/**
 * Service Abstraction Layer
 */
abstract class SAL {
	protected $_routes;
	protected $_auth;
	protected $_config;

	/**
	 * Inject dependencies
	 * @param Router $routes Route Object
	 * @param Auth $auth   Auth Object
	 * @param arraay $config Configuration Array
	 */
	public function __construct($routes, $auth, $config) {
		$this->_routes = $routes;
		$this->_auth = $auth;
		$this->_config = $config;
	}

	/**
	 * Perform Neccessary gatekeeper operations such as pars auth requirements / session check etc and then run the route.
	 * @return void Resource Response
	 */
	public function run() {
		if($this->auth()) {
			$this->_routes->run($this->_config);
		}
		else {
			$error = new Error();
			//Unauthorized
			$error->response(401);
		}
	}

	/**
	 * Perform Authentication based on AUTH object
	 * TODO: Decouple Session creation from Auth
	 * @return bool user permission
	 */
	protected function auth(){
		session_start();

		//Verify in session
		if(isset($_SESSION['user'])) {
			$permission = true;
		}
		//Are we loging in for the first time?
		//TODO Decouple routing from SAL
		else if($_SERVER['REQUEST_METHOD'] == 'POST' && $_SERVER['REQUEST_URI'] == $this->_config['prefix'].$this->_config['auth']) {
			//TODO Scrub and verify params
			//TODO Refactor: SAL should not be scrubbing file contents for JSON data.
			$params = json_decode(file_get_contents("php://input"), true);
			if($user = $this->_auth->authenticate($params['username'], $params['password'])) {
				$_SESSION['user'] = $user;

				$permission = true;
			}
			else {
				$permission = false;
			}
		}
		//else we do not have permission to the api
		else {
			$permission = false;
		}

		return $permission;
	}
}

/** EOF **/