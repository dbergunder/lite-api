<?php
/**
 * file api/library/Model.php
 *
 * Base class for Models
 */
namespace lite\api\library;

use lite\api\resources\Error as Error;

/**
 * Core methods inherited by all Models
 */
abstract class Model {
	protected $_db;
	protected $_config;

	public $query = '';

	public function __construct($config) {
		$this->_config = $config;
		$this->setupDB();
	}
	
	/**
	 * Executes Fetch All
	 * @return mixed Array or Error
	 */
	public function executeFetchAll() {
		//Execute Query
		if($this->query->execute()) {
			return $this->query->fetchAll(\PDO::FETCH_ASSOC);
		}
		else {
			//Need to return some kind of error message and status code
			return $this->query->debugDumpParams();
			//return false;
		}	
	}

	/**
	 * Executes Update
	 * @return mixed bool or Error
	 */
	public function executeUpdate() {
		if($this->query->execute()) {
			return true;
		}
		else{
			//Need to return some kind of error message and status code
			return $this->query->debugDumpParams();
		}	
	}

	/**
	 * Executes Insert
	 * @return mixed integer or Error
	 */
	public function executeInsert() {
		if($this->query->execute()) {
			//Return ID of last inserted item
			return $this->_db->lastInsertId(); 
		}
		else {
			//Need to return some kind of error message and status code
			return $this->query->debugDumpParams();
		}
	}

    /**
     * Opens Connection to Database
     */
    private function setupDB(){
        try{
            $this->_db = new \PDO("mysql:host=".$this->_config['host'].";dbname=".$this->_config['dbname']
                ,$this->_config['username']
                ,$this->_config['password']);

            $this->_db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->_db->setAttribute(\PDO::ATTR_STRINGIFY_FETCHES, false);
            $this->_db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            
        }
        catch (\PDOException $e) {
            //Debug purposes only, need to write to LOG file in future
            //error_log($e);
            $error = new Error();
            //Debug:
            $error->responseJSON($e->getMessage(), 500);
            
        }
    }

    /////Data PDO Preperation Functions/////
    
    /**
     * Prepares params array of Column => Value into PDO strings
     * @param  array $params Column=>Value pairs
     * @param  array $verifyColumns list of column names to verify against
     * @return array         Array of column = prepaired string, value = prepaired string
     */
    protected function prepareInsertColumnValues($params, $verifyColumns = null) {
    	$values = "'";
		$columns = "";

		foreach ($params as $key => $value) {
			//If verifyColumns is passed
			if(is_null($verifyColumns)) {
				$columns .= $key . ", ";
				$values .= $value . "', '";		
			}
			else {
				if(in_array($key, $verifyColumns)) {
					$columns .= $key . ", ";
					$values .= $value . "', '";
				}
				else {
					//Column doesn't match and abort update
					//TODO: Error that columns don't match
					return null;
				}				
			}

		}

		$columns = rtrim($columns, ", ");
		$values = rtrim($values, ", '");
		$values .= "'";

		return array('columns' => $columns, 'values' => $values);
    }

    /**
     * Prepares params array of Column => Value into PDO string
     * @param  array $params        Column=>Value paris
     * @param  array $verifyColumns list of column names to verify against
     * @return string                Prepaired string
     */
    protected function prepareUpdateColumnValues($params, $verifyColumns = null) {
		$columnValues = '';

		foreach ($params as $key => $value) {
			if(is_null($verifyColumns)) {
				$columnValues .= $key . " ='" . $value . "', ";
			}
			else {
				//Verify params consistancy
				if(in_array($key, $verifyColumns)) {
					$columnValues .= $key . " ='" . $value . "', ";
				}
				else {
					//Column doesn't match and abort update
					//TODO: Error that columns don't match
					return null;
				}
			}
			
		}
		//Trim off last comma
		$columnValues = rtrim($columnValues, ", ");

		return $columnValues;
    }
}

/** EOF */