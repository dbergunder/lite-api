<?php
/**
 * @file api/library/ResponseType.php
 * 
 * Generates a response format type
 * Contains node class, storage/SplObjectStorage class for tree sort
 */
namespace lite\api\library;

/**
 * Generate Response type for resources
 */
class ResponseType {
	//ENUM types for readability
	const COLLECTION = 1;
	const RESOURCE = 2;

	//Type of response to produce
	protected $type;
	//Keys to populate the meta data of the response
	protected $keys;

	/**
	 * Sets object for build type
	 * @param ResourceType::Const $type Type of response to generate
	 * @param array $keys keys to sort by
	 */
	public function __construct($type, $keys) {
		$this->type = $type;
		$this->keys = $keys;
	}

	/**
	 * Builds data tree
	 * @param  array $data linear data format provided from db
	 * @return array             Tree data format - multidimensional array
	 */
	public function build($data = null) {
		$dataTree = array();

		switch($this->type) {
			case(self::COLLECTION):
				$dataTree = $this->buildCollection($data);
				break;
			case(self::RESOURCE):
				$dataTree = $this->buildResource($data);
				break;
			default:	
				break;
		}

		return $dataTree;
	}

	/**
	 * Preprocess for building collection
	 * @param  array $data Data to build into collection type
	 * @return array       Collection formatted array
	 */
	private function buildCollection($data) {
		//List of keyList to compare against
		//$keyList = array_keys($data[0]);
		//$this->keys[$this->keys['type']] = $this->buildTree($data, $keyList);
		$this->keys[$this->keys['type']] = $data;
		return array('collection' => $this->keys);
	}

	/**
	 * Preprocess for building resource
	 * @param  array $data Data to build into resource type
	 * @return array       Resource formatted array
	 */
	private function buildResource($data = null) {
		if(!is_null($data)) {
			//List of keyList to compare against
			//if(is_array($data[0])) {
			//    $keyList = array_keys($data[0]);
			//}
			//$this->keys[$this->keys['type']] = $this->buildTree($data, $keyList);
			$this->keys[$this->keys['type']] = $data;
		}
		return array('resource' => $this->keys);
	}

	/**
	 * Build Tree formated Array, recursivly builds down each branch
	 *
	 * This is not working as intended, need a way to check for null values after initial loop
	 * because this will insert strange data
	 * 
	 * @param  array  $data   Preformated array
	 * @param  array  $keyList List of keys to compare
	 * @return array          Formatted tree array
	 */
	private function buildTree(&$data, $keyList) {
		//New SplObjectStorage type
		$branch = new storage();

		//Shift off next value from list
	    $keyName = array_shift($keyList);

	    //Verify we are working with good data
	    if(empty($data)) {
	    	return;
	    }

	    if(is_null($keyName)) {
	    	return $data;
	    }

	    //Loop through every array set in the Data Array
    	foreach($data as $key => &$rowArray) {

    		//SplObjectStoarge uses objects as Keys - check as unique identifier for key->value pairs
    		$branchKey = (object)array($keyName => $rowArray[$keyName]);

    		//Branch entry already exists
    		if(isset($branch[$branchKey]) && $branch[$branchKey]->getValue() == $rowArray[$keyName]) {
	    		unset($rowArray[$keyName]);
	    		$branch[$branchKey]->addChild($rowArray);
    		}
    		//Does not exist and check for matching
    		else if(isset($data[1+$key]) && $rowArray[$keyName] == $data[1+$key][$keyName]) {
    			$branch[$branchKey] = new node($keyName, $rowArray[$keyName]);
    			unset($rowArray[$keyName]);
    			unset($data[1+$key][$keyName]);
    			$branch[$branchKey]->addChild($rowArray);
    			$branch[$branchKey]->addChild($data[1+$key]);
    		}
    		//Does not match add new branch
    		else {
				$branch[$branchKey] = new node($keyName, $rowArray[$keyName]);
    			unset($rowArray[$keyName]);
    			$branch[$branchKey]->addChild($rowArray);
    		}
    	}

    	//Make sure there are still keys to compare before submitting
    	if(!empty($keyList)) {
			foreach($branch as $branchKey) {
				$branch[$branchKey]->setChildren($this->buildTree($branch[$branchKey]->getChildren(), $keyList));
			}
		}
    	
    	//Return sorted branch
    	return $branch;
	}

}

/**
 * Storage extends SplObjectStorage in order to implement JsonSerializable
 *
 * For displaying data as JSON object
 */
class storage extends \SplObjectStorage implements \JsonSerializable {
	public function jsonSerialize() {
		$return = array();
		foreach($this as $object) {
			$return[] = array($object, $this[$object]);
		}
		return $return;
	}
}

/**
 * Builds Tree like Hierarchy
 */
class node implements \JsonSerializable { //extends SplDoublyLinkedList {
	public $name;
	public $value;
	public $children = array();

	public function __construct($name, $value) {
		$this->name = $name;
		$this->value = $value;
	}

	/**
	 * Get Node Data
	 * @return mixed 
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Set Node Data
	 * @param mixed $value
	 */
	public function setValue($value) {
		$this->value = $value;
	}

	/**
	 * Add Node
	 * @param mixed $child 
	 */
	public function addChild($child) {
		$this->children[] = $child;
	}

	/**
	 * Return Child by Key
	 * @param  mixed $key Key
	 * @return mixed 
	 */
	public function getChild($key) {
		return $this->children[$key];
	}

	/**
	 * Return all keys for Children array
	 * @return array keys
	 */
	public function getChildKeys() {
		return array_keys($this->children);
	}

	/**
	 * Return all children
	 * @return array $children
	 */
	public function getChildren() {
		return $this->children;
	}

	/**
	 * Set Children array
	 * @param array $children 
	 */
	public function setChildren($children) {
		$this->children = $children;
	}

	/**
	 * How the object will be serialized by json
	 * @return array 
	 */
	public function jsonSerialize() {
		return $this->children;
	}
}

/** EOF **/