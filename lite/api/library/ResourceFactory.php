<?php
/**
 * @file api/library/ResourceFactory.php
 * 
 * Factory pattern for resources
 */
namespace lite\api\library;

/**
 * Resource Factory Pattern
 */
class ResourceFactory {

	/**
	 * Create new Resource
	 * Perform any neccessary resource steps
	 * @param  string $resource Resource Object to create
	 * @param  array $config   Configuration Array
	 * @return resource           Generated Resource
	 */
    public static function create($resource, $config) {
    	//Dynamically Build Resource Object
    	$resource = 'lite\api\resources\\'.$resource;
        return new $resource($config);
    }
}

/** EOF **/