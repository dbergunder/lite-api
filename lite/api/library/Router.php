<?php
/**
 * @file api/library/Router.php
 * @description API Router
 *
 * Stores and Routes API paths which are set in the index.php
 * Callbacks can be anonymous functions, normal functions, objects or files
 */
namespace lite\api\library;
//Exception handler
set_exception_handler(array('lite\api\library\Router','exception'));

use lite\api\library\ResourceFactory as ResourceFactory;
use lite\api\resources\Error as Error;

/**
 * Stores routes and matches requests
 */
class Router {

  /** Stored Routes */
  protected $_routes = array();

  /** URI to match */
  protected $_uri;

  /** Configuration Array */
  protected $_config;

  /**
   * Router Constructor
   * @param string $uri Hardcoded URI string for testing
   */
  public function __construct($uri = null) {
    if($uri === NULL) {
      /*
      //TODO: Test change on production server
      $uri = explode('/',$_SERVER['REQUEST_URI']);
      array_shift($uri);
      $this->_uri = implode('/', $uri);
      */
      //Build URI to match, remove possible query string
      $this->_uri = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']); 
    }
    else{
      $this->_uri = $uri;
    }
  }

  /**
   * Stores HTTP Method API Route mapped to controller
   * @param  string $method HTTP Method
   * @param  string $pattern  API Route to be mapped
   * @param  mixed $resourceName Type of Resource
   * @param  string|null $resourceMethod Method to be called on Resource
   */
  public function addRoute($httpMethod, $pattern, $resourceName, $resourceMethod = null) {
    $this->_routes[strtoupper($httpMethod)][] = array(
                                                  'pattern' => $pattern, 
                                                  'resource' => $resourceName, 
                                                  'method' => $resourceMethod 
                                                  );
  }

  /**
   * Matches API Resrouce Request to Resource for Execution
   * @param  array $config Configuration Array
   * @return mixed HTTP 404/405
   */
  public function run($config = null){
    //Update Config
    $this->_config = $config;

  	//Return Error Response if HTTP method does not match
  	if (!array_key_exists($_SERVER['REQUEST_METHOD'], $this->_routes)) {
      //Error status Method not Allowed
      $response = new Error();
      return $response->response(405);
    }

  	//Search for stored pattern and execute if matches
  	foreach($this->_routes[$_SERVER['REQUEST_METHOD']] as &$route) {

  		//convert URL parameter (e.g. ":id", "*") to regular expression
  		$regex = preg_replace('#:([\w]+)#', '(?<\\1>[^/]+)',
  	      str_replace(array('*', ')'), array('[^/]+', ')?'), $route['pattern']));
      if (substr($route['pattern'],-1)==='/') $regex .= '?';

      //extract parameter values from URL if route matches the current request
      if (preg_match('#^'.$regex.'$#', $this->_uri, $values)) {
        //extract parameter names from URL
        preg_match_all('#:([\w]+)#', $route['pattern'], $params, PREG_PATTERN_ORDER);

        $args = array();
        foreach ($params[1] as $param) {
          if (isset($values[$param])) $args[$param] = urldecode($values[$param]);
        }
        //Resource matched, build object and execute method with arguments
        $this->_exec($route['resource'], $route['method'], $args);
      }
  	}

  	//Fall through return Error Response
    $response = new Error();
  	return $response->response(404);
  }

  /**
   * Executes matched API callback
   * @param  mixed $callback Code to be executed
   * @param  array $args     Arguements passed to execution
   */
  protected function _exec(&$resource, &$method, &$args) {
    if(is_string($resource)) {
      $r = ResourceFactory::create($resource, $this->_config);
      $r->$method($args);
    }
    else {
      foreach ((array)$resource as $cb) call_user_func_array($cb, $args);      
    }
    // Halt instead of exit()
    throw new Halt();
  }

  /**
   * Stop execution on exception and log as E_USER_WARNING
   * @param  exception $e 
   */
  public static function exception($e) {
    if ($e instanceof Halt) return;
    trigger_error($e->getMessage()."\n".$e->getTraceAsString(), E_USER_WARNING);
    $error = new Error();
    //TODO log and send custom error
    $error->responseJSON($e->getMessage(), 500);
  }

}

/**
 * use Halt-Exception instead of exit
 */
class Halt extends \Exception {}

/** EOF **/
