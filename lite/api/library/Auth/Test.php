<?php
/**
 * @file api/library/Auth/Test.php
 *
 * Stub Implementation of Test Login
 */
namespace lite\api\library\AuthAdapter;

use lite\api\library\AuthAdapter as AuthAdapter;
/**
 * Implements external Test Authentication
 */
class Test implements AuthAdapter {
	public function authenticate($username, $password){
		//return array('user' => 'Test User');
		return true;
	}

    public function identify(){
        return __CLASS__;
    }
}

/** EOF */