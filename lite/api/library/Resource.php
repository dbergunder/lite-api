<?php
/**
 * @file api/library/Resource.php
 *
 * Base abstract resource class that defines API logic and responses
 */
namespace lite\api\library;

use lite\api\library\ResponseType as ResponseType;

/**
 * Base abstract class for all Resource objects
 */
abstract class Resource {

    /** Configuration Array */
	protected $_config = array();

    /** Search Filters */
	protected $_filters = array();

    /** JSON passed params (Post/Put) */
	protected $_params = array();

    /**
     * Constructor checks for filters, params, and sets the configuration internally
     * @param array $config Configuration Array
     */
	public function __construct($config = null) {
		$this->_config = $config;

		if(!empty($_GET)) {
            //API filters
			$this->_filters = &$_GET;
		}

        //If POST/PUT Get Params from JSON
        if($_SERVER['REQUEST_METHOD'] == ('POST' || 'PUT')) {
            $this->_params = json_decode(file_get_contents("php://input"), true);

            //TODO: uncomment this if _params contains JSON data DATE info
/*            if(is_array($this->_params)) {
                array_walk($this->_params, function($key, $value) {
                    //If the key string contains _date, marking it as a date field
                    if(strpos($key, '_date') !== FALSE) {
                        return strtotime($value);
                    } 
                });
            }   */
        }
	}

	/**
	* Displays a simple HTML response
	* @param  string $status   Header Status
	*/
	public function response($status = 400) {
		header('HTTP/1.1 ' . $status . ' ' . $this->_requestStatus($status));
		return true;
	}

    /**
     * Generates a JSON response
     * @param  mixed  $data   Information to Encode
     * @param  integer $status HTTP header status
     * @return bool
     */
	public function responseJSON($data, $status = 200) {
		header('HTTP/1.1 ' . $status . ' ' . $this->_requestStatus($status));
		header('Content-Type: application/json; charset=utf-8');
        echo json_encode($data, JSON_NUMERIC_CHECK);
		//echo json_encode($data);
		return true;
	}

    /**
     * Build a collection response for JSON output
     * @param  keys  $keys   Keys of collection
     * @param  array  $data   Data to generate into collection
     * @param  integer $status HTTP header status
     * @return void          
     */
  public function responseCollectionJSON($keys, $data, $status = 200) {
        $response = new ResponseType(ResponseType::COLLECTION, $keys);
        $collection = $response->build($data);
    	$this->responseJSON($collection, $status);
    }

    /**
     * Build a resource response for JSON output
     * @param  keys  $keys   Keys of resource
     * @param  array  $data   Data to generate into resource
     * @param  integer $status HTTP header status
     * @return void 
     */
    public function responseResourceJSON($keys, $data = null, $status = 200) {
        $response = new ResponseType(ResponseType::RESOURCE, $keys);
        $resource = $response->build($data);
        $this->responseJSON($resource, $status);
    }

    /**
     * Geberates an XML response
     * @return bool
     */
	public function responseXML(){
        return true;
    }

	/**
     * Cleans Data for Input
     * 
     * @param array $data
     * @return array Clean Input
     */
    private function _cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                if($k!='request'){
                    $clean_input[$k] = $this->_cleanInputs($v);
                }
            }
        } 
        else {
            $clean_input = trim(strip_tags($data));
        }

        return $clean_input;
    }

	/**
     * Associates status code to status message
     * 
     * @param int $code Status Code
     * @return string Status Message
     */
    private function _requestStatus($code) {
        
        $status = array( 
            100 => 'Continue',   
            101 => 'Switching Protocols',   
            200 => 'OK', 
            201 => 'Created',   
            202 => 'Accepted',   
            203 => 'Non-Authoritative Information',   
            204 => 'No Content',   
            205 => 'Reset Content',   
            206 => 'Partial Content',   
            300 => 'Multiple Choices',   
            301 => 'Moved Permanently',   
            302 => 'Found',   
            303 => 'See Other',   
            304 => 'Not Modified',   
            305 => 'Use Proxy',   
            306 => '(Unused)',   
            307 => 'Temporary Redirect',   
            400 => 'Bad Request',   
            401 => 'Unauthorized',   
            402 => 'Payment Required',   
            403 => 'Forbidden',   
            404 => 'Not Found',   
            405 => 'Method Not Allowed',   
            406 => 'Not Acceptable',   
            407 => 'Proxy Authentication Required',   
            408 => 'Request Timeout',   
            409 => 'Conflict',   
            410 => 'Gone',   
            411 => 'Length Required',   
            412 => 'Precondition Failed',   
            413 => 'Request Entity Too Large',   
            414 => 'Request-URI Too Long',   
            415 => 'Unsupported Media Type',   
            416 => 'Requested Range Not Satisfiable',   
            417 => 'Expectation Failed',   
            500 => 'Internal Server Error',   
            501 => 'Not Implemented',   
            502 => 'Bad Gateway',   
            503 => 'Service Unavailable',   
            504 => 'Gateway Timeout',   
            505 => 'HTTP Version Not Supported');
             
        return ($status[$code])?$status[$code]:$status[500]; 
    }
}

/** EOF **/
