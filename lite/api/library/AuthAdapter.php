<?php
/**
 * @file library/AuthAdapter.php
 *
 * Interface for Authentication Adapters
 */
namespace lite\api\library;

/**
 * Interface for Authentication Adapters
 */
interface AuthAdapter {

	/**
	 * All AuthAdapter has access to an authentication method
	 * @param  string $username
	 * @param  string $password
	 * @return mixed
	 */
	public function authenticate($username, $password);

	/**
	 * All AuthAdapter has an identifier
	 * @return string Class Name
	 */
	public function identify();
}

/** EOF */

