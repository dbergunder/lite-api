<?php
/**
 * Base class for external lite requests
 */
namespace lite\api\library\SAL;

use lite\api\library\SAL as SAL;

/**
 * External Service Request
 */
class EXTERNAL extends SAL {
	
}

/** EOF **/