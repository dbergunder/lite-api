<?php
/**
 * Base class for internal lite api requests
 */
namespace lite\api\library\SAL;

use lite\api\library\SAL as SAL;

/**
 * Internal Service Request
 */
class INTERNAL extends SAL {
}

/** EOF **/