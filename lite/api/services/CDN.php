<?php
/**
 * @file api/services/CDN.php
 *
 */
namespace LMP\api\services;

use LMP\api\library\Service as Service;

/**
 * Content Delivery Network Service
 */
class CDN extends Service {
	public function __construct($config){

		//Call parent constructor
		parent::__construct($config);
	}

	public function postFile($params) {
		// Post external file, expect URL return
	}

	public function putFile($params) {
		// Requires original File Location
		// Update external File
	}
}

/** EOF **/